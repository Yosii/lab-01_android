//exercise number 5
fun main(args: Array<String>) {
    print("Digite un número ")
    var num1 = readLine()?.toInt() as Int

    print("Digite otro número ")
    var num2 = readLine()?.toInt() as Int

    //This method is done so that the number becomes negative to solve the exercise using subtraction
    var neg2 = num2 - num2*2

    var suma = num1 - neg2
    print("Número 1 = $num1\n")
    print("Número 2 = $num2\n")
     print("La suma de los dos números es: $suma")
}