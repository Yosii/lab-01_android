//exercise number 2

fun main(args: Array<String>) {
    var x = 13
    var i = 0

    // A while was used to reduce the hour and with a counter to add degrees.
    print("Fórmula de conteo regresiva:")
    while (x > 8) {
        print("X => ${x--}\t")
        i++
    }
    println("")
    print("Las agujas del reloj se devuelven $i grados")
}