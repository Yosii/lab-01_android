//exercise number 1
fun main(args: Array<String>) {
    //Formula raised in the problem
    print("Fórmula Planteada: (((x+9)*3)-5)/4=7 \n")
    //Formula applied
    print("Fórmula Aplicada: x=(((7*4)+5)/3)-9\n")
    //This is how the problem was solved, solving for x
    var x = (((7*4)+5)/3)-9
    print("El valor de x es: $x")
}