//exercise number 3
import kotlin.math.abs
fun main(args: Array<String>) {
    //In the first function abs was used to obtain the smallest number
    print("----------------\n")
    print("|   FUNCIÓN 1  |\n")
    print("----------------\n")
    //Capturing data from console
    print("Digite un número ")
    var num1 = readLine()?.toInt() as Int

    print("Digite otro número ")
    var num2 = readLine()?.toInt() as Int
    var mayor = abs((num1+num2)+ abs((num1-num2)))/2
    var menor = abs((num1+num2)- abs((num1-num2)))/2

    print("Número 1: $num1 \n")
    print("Número 2: $num2 \n")
    print("El número menor es: $menor \n")


    //In the first function, minOF was used to obtain the smallest number
    print("----------------\n")
    print("|   FUNCIÓN 2  |\n")
    print("----------------\n")
    print("Digite un número ")
    var n1 = readLine()?.toInt() as Int

    print("Digite otro número ")
    var n2 = readLine()?.toInt() as Int
    var menor2= minOf(n1,n2)
    print("El número menor es: $menor2")











}
